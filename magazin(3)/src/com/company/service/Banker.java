package com.company.service;

import com.company.departments.BaseDepartment;
import com.company.bank.BaseBank;

public class Banker extends BaseEmployee{
    private boolean free;
    private BaseBank bank;

    public Banker(String name, boolean free, BaseDepartment department, BaseBank bank) {
        super(name, free, department);
        this.bank = bank;
    }

    public void sendRequest(){}

    public boolean isFree(){
        return free;
    }
}
