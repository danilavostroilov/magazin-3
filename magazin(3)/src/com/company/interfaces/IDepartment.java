package com.company.interfaces;

import java.util.ArrayList;

public interface IDepartment {
    String getName();
    ArrayList<IGood> getGoodList();
    ArrayList<IEmployee> getEmployeeList();
    void addGood();
    void addEmployee();
}

