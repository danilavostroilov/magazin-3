package com.company.interfaces;

public interface IBank {
    void checkInfo();
    void giveCredit();
    String getName();
    String getCreditDescription();
}


