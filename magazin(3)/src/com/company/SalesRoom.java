package com.company;

import com.company.clients.BaseVisitor;
import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IVisitor;

import java.util.ArrayList;

public class SalesRoom {
    private String name;
    ArrayList <IVisitor> BaseVisitor;
    ArrayList <IDepartment> BaseDepartment;

    public SalesRoom(String name, ArrayList<IVisitor> baseVisitor, ArrayList<IDepartment> baseDepartment) {
        this.name = name;
        BaseVisitor = baseVisitor;
        BaseDepartment = baseDepartment;
    }

    public SalesRoom() {

    }

    public SalesRoom(String name) {
        this.name = name;
    }

    public void addDepartment(BaseDepartment department){
        BaseDepartment.add(department);
    }
    public void addVisitor(){

    }
}

